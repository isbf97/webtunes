<?
/* AddUser: Adds a user to the system. Copied from BSAuthComplex */
require('../config.php'); 
	if(!isset($_SESSION)) {
	session_start();
	}
if($_SESSION['uid'] != 'a') { // Disallow non-admin
	header('Location: ../index.php');
	die();
}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">

<meta charset="utf-8">

    <script src="../jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
<title>Add</title>
 <!-- Le styles -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      /* Override some defaults */
      html, body {
        background-color: #eee;
        height:100%;
      }
      body {
        padding-top: 40px; 
      }
      .container {
        width: 400px;
      }

      /* The white background content wrapper */
      .container > .content {
        background-color: #fff;
        padding: 20px;
        margin: 0 -20px; 
        -webkit-border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
                border-radius: 10px 10px 10px 10px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                box-shadow: 0 1px 2px rgba(0,0,0,.15);
      }

	  .login-form {
		margin-left: 65px;
	  }
	
	  legend {
		margin-right: -50px;
		font-weight: bold;
	  	color: #404040;
	  }
	  
	  input[type="text"]:disabled {
		 color: #000;
		 background-color: #fff;
	  }

    </style>
   </head>
<body >


<?php

$act=$_POST['act'];
$tgt=$_GET['tgt'];
$acturl = $_GET['act'];
if($act == '' && $acturl=='') {
// No action, render an edit/add form
$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
mysql_select_db($dbname, $dlink);
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
$result = mysql_query("SELECT * FROM logins WHERE id=".$tgt." ORDER BY id ASC", $dlink);
$editing= mysql_fetch_array($result);

	echo "<div class=\"container\" style=\"width:600px;\">
    <div class=\"content\">
      <div class=\"row\">
        <div class=\"login-form\">";

	
          echo "<h2>User control</h2>
          <h4>Enter user details</h4>";
       echo   "<form action=\"addUser.php\" method=\"POST\">
            <fieldset>
               <label>Login:</label><input name=\"name\" type=\"text\" placeholder=\"Login\" value=\"".$editing['name']."\"><br><label>Password:</label><input name=\"newpass\" type=\"password\" placeholder=\"New password (if need to change)\" value=\"\">
               <br>";

             if(trim($tgt) == ''){
	              echo "    <input type=\"hidden\" name=\"act\" value=\"add\">";
             } else {
	              echo "    <input type=\"hidden\" name=\"act\" value=\"change\">";
	               echo "    <input type=\"hidden\" name=\"ident\" value=\"".$tgt."\">";
             }
           
            
             echo "   <div class=\"clearfix\">
              <button class=\"btn btn-primary\" id=\"sub\" type=\"submit\" >Save</button>
              <a class=\"btn\" href=\"usercontrol.php\">Cancel</a></div>
            </fieldset>
          </form>
";


	echo "</div></div></div></div>";
mysql_close($dlink);

}

if($act == 'add') {
// Add to database

$user=htmlspecialchars($_POST['name']);
$pasw=md5($_POST['newpass']);
$timez=intval($_POST['timezone']);
if(trim($user)==='') {
	die('<div class="alert alert-block">No login specified.</div>');
}
if(trim($_POST['newpass'])==='') {
	die('<div class="alert alert-block">No password specified.</div>');
}
$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
mysql_select_db($dbname, $dlink);
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
mysql_query("Insert into logins (name,pass) values ('".mysql_escape_string(htmlspecialchars($user))."','".mysql_escape_string($pasw)."')", $dlink);

mysql_close($dlink);
echo "<meta http-equiv=\"refresh\" content=\"0; url=usercontrol.php\"> ";

}
if($act == 'change') {
// Rewrite in database
$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
mysql_select_db($dbname, $dlink);
$user=htmlspecialchars($_POST['name']);
$pasw=md5($_POST['newpass']);
$timez=intval($_POST['timezone']);
if(trim($user)==='') {
	die('<div class="alert alert-block">Login not specified.</div>');
}
mysql_query ("set character_set_client='utf8'", $dlink); 
mysql_query ("set character_set_results='utf8'", $dlink); 
mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
mysql_query("update logins set name='".mysql_escape_string(htmlspecialchars($name))."' where id=".$ident, $dlink);
if(trim($_POST['newpass'])!='') {
	mysql_query("update logins set pass='".$pasw." where id=".$ident, $dlink);
}
mysql_close($dlink);
echo "<meta http-equiv=\"refresh\" content=\"0; url=usercontrol.php\"> ";

}

if($acturl=='del') {
// Delete from database
$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
mysql_select_db($dbname, $dlink);
	mysql_query("Delete from logins where id=".mysql_escape_string($tgt), $dlink);
	mysql_query("Delete from playlists where owner=".mysql_escape_string($tgt), $dlink);
	
	mysql_close($dlink);
echo "<meta http-equiv=\"refresh\" content=\"0; url=usercontrol.php\"> ";
}
?>
</body></html>
