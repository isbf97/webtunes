//General Document Init
$(document).ready(function()  {
prepareLocale();
//$('#addbtn').click(function(evt) { makeList(evt); });
document.getElementById('footer-process').style.display="none"; // Hide progress bar
$( ".volume-bar" ).volslider({ // Create a volume slider
      value:100,
      min: 0,
      max: 100,
      step: 5,
      slide: function( event, ui ) { // Volslider code
      	if(window.player == undefined && window.audio == undefined) return;
        if(window.player != undefined) window.player.volume = ui.value ;
        if(window.audio != undefined) window.audio.volume = ui.value / 100;
      }
    });
   $( ".track-progress" ).seekslider({ // Make a seek slider
      value:0,
      min: 0,
      max: 1000,
      step: 1,
      slide: function( event, ui ) { // seek slider binding
      	if(window.player == undefined) return;
      	if(window.player.buffered < 100) {
	      	$( ".track-progress" ).seekslider('option','value',(window.player.currentTime / window.player.duration)*1000);
	      	return;
      	}
        window.player.seek(Math.round((window.player.duration / 1000) * ui.value));
        
      }
    });
 $('#fileupload').fileupload({ // File upload plugin -- drop to window for upload
        start: function() {popMessage(window.locale.errors.uploading);}, // start notify
        done: function (e, data) {
        // On done
        if(data.response().result == 'disallow') { popMessage(window.locale.errors.guestUpload); return; } // disallowed
        popMessage(window.locale.errors.uploadSuccess); // All fine
        disk(); // Disk space indication
        if(data.response().result.split(':')[0] != 'ZIP' ){
	      // no zip, it was unsorted
           invalidateUI('unsorted');
        } else {
        	// uncompress zip via form
	        	performZipDecomp(data.response().result);
         }
           
           
        },
        fail: function (e, data) { // upload error
           console.log(data.response().result);
           
           popMessage(window.locale.errors.uploadFail);
        },
        progressall: function (e, data) { // progress bar
        var progress = parseInt(data.loaded / data.total * 100, 10);
          document.getElementById('progressbar-footer').style.width=progress.toString()+"%";
			document.getElementById('footer-process').style.display="block";
			document.getElementById('process-sig').innerHTML=window.locale.ui.uploading;
			if(progress == 100) { 
				document.getElementById('footer-process').style.display="none";
			}
    }
    });
    	if(window.starterUI == undefined && window.uid=='g') { // No start ui linked, neither a user was logged in. purely UX hack, user control works w/o it too
	    	alert(window.locale.errors.login);
	    	window.location='index.php';
    	}
   //    if(window.starterUI == undefined) window.starterUI = 'music';

    if(window.starterUI!= undefined){ // starter ui process
	    switch(window.starterUI) {
	    	case 'album':
	    		 showOneAlbum(window.starterAlbum);
	    		 break;
	    	case 'user':
	    		showUser(window.starterUser);
	    		break;
	    		
	    	default:
	    		switchTo(window.starterUI);
	    		 
    	}
    }	

    $( "#dialog-form" ).dialog({ // Make a save album form for unzipping
      autoOpen: false,
      height: 300,
      width: 350,
      modal: true,
      buttons: {
        "Save this album": function() {
        	 $.ajax({type:"POST", url:'albummaker.php', data:{"zipname": $('#zipname')[0].value, 'album': $('#album')[0].value,'artist':$('#artist')[0].value,'compilation':$('#isCompilation')[0].checked.toString()} , success:function(data) {
						popMessage(window.locale.errors.albumSaved);	
						invalidateUI('music');
						loadSidebar();
								
					}});
        	 $( this ).dialog( "close" );
        	
         },
        "Discard this ablum": function() {
        	
          	$.ajax({type:"POST", url:'unlinkazipa.php', data:{"zipname": $('#zipname')[0].value} , success:function(data) {
						popMessage(window.locale.errors.albumSaved);		
						}		
					});
					$( this ).dialog( "close" );
        }
      },
      close: function() {
      	$.ajax({type:"POST", url:'unlinkazipa.php', data:{"zipname": $('#zipname')[0].value} , success:function(data) {
						popMessage(window.locale.errors.albumSaved);		
						}		
					});

        disk();
      }
    });
    if($('#linkalb-form')[0] != undefined) { // Make symlink form too
	      $( "#linkalb-form" ).dialog({
      autoOpen: false,
      height: 300,
      width: 350,
      modal: true,
      buttons: {
        "Save this album": function() {
        	 $.ajax({type:"POST", url:'symlinker.php', data:{'kind':'album',"from": $('#folder-sy')[0].value, 'album': $('#album-sy')[0].value,'artist':$('#artist-sy')[0].value,'compilation':$('#isCompilation-sy')[0].value} , success:function(data) {
						popMessage(window.locale.errors.albumSaved);	
					
						loadSidebar();
							invalidateUI('music');
					}});
        	 $( this ).dialog( "close" );
        	
         },
       "Discard this album": function() {
        		$( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    }
 
  if($('#linktrk-form')[0] != undefined) { // single track symlink form as well :3
	      $( "#linktrk-form" ).dialog({
      autoOpen: false,
      height: 300,
      width: 350,
      modal: true,
      buttons: {
        "Link this track": function() {
        	if(window.curUI != 'unsorted' && window.curUI.substr(0, 3) != 'alb') {
	        	popMessage(window.locale.errors.linkError);
	        	return;
        	}
        	 $.ajax({type:"POST", url:'symlinker.php', data:{"from": $('#file-st')[0].value, 'to': window.curUI, 'kind': 'single'} , success:function(data) {
						popMessage(window.locale.errors.saved);	
					
						loadSidebar();
								invalidateUI('music');
					}});
        	 $( this ).dialog( "close" );
        	
         },
        "Cancel": function() {
        	
					$( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
    }
    
if(window.uid==='g') { // for guest, no sidebar
	$('#listing-container')[0].style.width='100%'; $('#listing-container')[0].style.left='0'; 
	$('#csearch')[0].style.display='none';
}

loadSidebar(); // load sidebar
    
});
