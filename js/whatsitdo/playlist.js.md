# playlist.js
## contains common playlist operations

* `renameList()` calls the ui and then renames the currently viewed playlist, is a handler for the "Rename" button
* `shareList()` builds and shows an invite link to the currently viewed playlist
* `trashList()` deletes the currently viewed playlist
* `copyList(list)` creates a copy of the playlist under the specified identifier into the current user's account
* `makeList()` calls up the name input box and then creates a new playlist by the user-specified name
* `updateProg()` updates the current track progress, that is time and seek slider
* `disableProg()` resets the current track slider and time to 'unknown' values, that is, the slider being in the beginning, and the times being `--:--`
