

// --- Drag Drop Routines

function allowDrop(ev)
{ // Allow drag drop
	ev.preventDefault();
	ev.target.className = 'dropin';
}
function noDrop(ev) {
	// Cancel drop
	ev.preventDefault();
	ev.target.className = 'dropout';
}

function drag(ev)
{
	// Drag out
	console.log("drag");
	ev.dataTransfer.setData("Text",ev.target.id);
}
var lastDrag=null; var lastDrop=null;
function drop(ev)
{ // Drop somewhere
	ev.preventDefault();
	var fname=ev.dataTransfer.getData("Text");
	
	console.log("drop "+fname+" to "+ev.target.parentNode.id);
	lastDrag=ev.target.parentNode.id;
	lastDrop=fname;
	ev.target.className = 'dropout';
	invalidateUI(lastDrag);
	$.ajax({type:"POST", url:'mousedroppings.php', contentType:'application/x-www-form-urlencoded; charset=UTF-8', data:{"file":encodeURIComponent(fname), "list":ev.target.parentNode.id} , success:function(data) {
					console.log('MD: '+data);
					if(data == 'disallow') { popMessage(window.locale.errors.notYourList); return;}
					if(data == 'done') {
						document.getElementById(lastDrag).style.background = '#afa';
						setTimeout(function() {document.getElementById(lastDrag).style.background = ''; }, 200);
						if(lastDrag === window.playingFromUI) {
							window.queue.push(lastDrop);
						}
						invalidateUI(lastDrag);
					}
					
				}});
}

function dropSC(ev)
{ // Drop on soundcloud
	ev.preventDefault();
	var fname=ev.dataTransfer.getData("Text");
	var tid = fname.substr(2, fname.length);
	console.log("drop "+fname+" to "+ev.target.parentNode.id);
	lastDrag=ev.target.parentNode.id;
	lastDrop=fname;
	ev.target.className = 'dropout';
	var betterdest;
	if(lastDrag.substr(0, 3) == 'scl') {
		betterdest = lastDrag.substr(4, lastDrag.length);
		console.log(betterdest);
	} else {
		betterdest = lastDrag.substr(3, lastDrag.length);
	}
	invalidateUI(lastDrag);
	$.ajax({type:"GET", url:'sc_auth.php', data:{"tid":tid, "dest":betterdest, "act":"drop"} , success:function(data) {
					console.log('MD: '+data);
					if(data == 'done') {
						document.getElementById(lastDrag).style.background = '#afa';
						setTimeout(function() {document.getElementById(lastDrag).style.background = ''; }, 200);
						if(lastDrag === window.playingFromUI) {
							window.queue.push(lastDrop);
						}
						invalidateUI(lastDrag);
					} else {
						popMessage(window.locale.errors.fail);
					}
					
				}});
}
function allowDropTrash(ev)
{ // Allow drop on trash
	ev.preventDefault();
}
function noDropTrash(ev) { // Cancel drop on trash
	ev.preventDefault();
}

var lastDrag=null;
function dropToTrash(ev)
{ // Drop to trash
	ev.preventDefault();
	var fname=ev.dataTransfer.getData("Text");
	lastDrag = fname;
	console.log("drop "+fname+" to trash from "+window.curUI);
	
	switch(window.curUI) {
		case 'music':
			$.ajax({type:"POST", url:'killitem.php', data:{"kind":'music',"item":fname.split('/')[2], "from":fname.split('/')[1]} , success:function(data) {
					$(document.getElementById(lastDrag)).remove();
					disk();
					invalidateUI(window.curUI);
					invalidateUI('alb'+fname.split('/')[1]);
				}});
		break;
		
		case 'unsorted':
			$.ajax({type:"POST", url:'killitem.php', data:{"kind":'unsort',"item":fname.split('/')[2]} , success:function(data) {
					$(document.getElementById(lastDrag)).remove();
					disk();
					invalidateUI(window.curUI);
				}});
		break;
		
		case 'compilation':
			$.ajax({type:"POST", url:'killitem.php', data:{"kind":'music',"item":fname.split('/')[2], "from":fname.split('/')[1]} , success:function(data) {
					$(document.getElementById(lastDrag)).remove();
					disk();
					invalidateUI(window.curUI);
				}});
		break;
		
	
		
		default:
		if(window.curUI.substr(0, 3) == 'alb') {
			$.ajax({type:"POST", url:'killitem.php', data:{"kind":'music',"item":fname.split('/')[2], "from":fname.split('/')[1]} , success:function(data) {
					$(document.getElementById(lastDrag)).remove();
					disk();
					invalidateUI(window.curUI);
					invalidateUI('music');
					invalidateUI('compilation');
				}});
				return;
		} 
		if(window.curUI.substr(0, 2) == 'sc' && window.curUI.substr(0, 3) != 'scs' && window.curUI != 'sc_feed' && window.curUI != 'sc_own') {
		console.log('thrash SC');
			$.ajax({type:"GET", url:'sc_auth.php', data:{"act":"trash","tid":fname.substring(2, fname.length), "from":window.curUI.substr(3, window.curUI.length).replace('_', '')} , success:function(data) {
					$(document.getElementById(lastDrag)).remove();
					console.log(data);
					console.log('thrash SC done');
					disk();
					invalidateUI(window.curUI);
					if(data != 'trashed') {
						
						popMessage(data);
					}
				}});
				return;
		} 
		
				$.ajax({type:"POST", url:'killitem.php', data:{"kind":'list',"item":fname, "from":window.curUI} , success:function(data) {
				//	switchTo(window.curUI);
				$(document.getElementById(lastDrag)).remove();
				disk();
				}});
		
	
		break;
	}
	var i = window.queue.indexOf(fname);
	if(i > -1) {
		delete window.queue[i];
		window.queue = window.queue.filter(function(n) { return n;});
	}
}

//-------- End drop routines
