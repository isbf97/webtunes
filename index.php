<? require 'config.php'; 
if(!isset($_SESSION)) {
	session_start();
}
if($_SESSION['uid'] != 'g') { // If logged in, go to player straight ahead
	header('Location: player.php');
}
// Generic login routine
$fail = 0;
$pass = $_POST['pass'];
$login = $_POST['login'];

if(trim($login) != '' && trim($pass) != ''){
	$pass= md5($pass);
	if($login == $admin && $pass == $admpass) {
		$_SESSION['hasLoggedIn']=1;
		$_SESSION['uid']='a';
		header('Location: player.php');
	
		die();
	}
	$dlink = mysql_connect($dbhost,$dbuser,$dbpass, true, 0);
	mysql_select_db($dbname, $dlink);
	mysql_query ("set character_set_client='utf8'", $dlink); 
	mysql_query ("set character_set_results='utf8'", $dlink); 
	mysql_query ("set collation_connection='utf8_general_ci'", $dlink); 
	$q = "SELECT * FROM logins WHERE name LIKE '".$login."' AND pass LIKE '".$pass."'";
	$result = mysql_query($q, $dlink);
	$row = mysql_fetch_array($result);
	$thing = mysql_num_rows($result);
	if($thing == 1) {
		$_SESSION['hasLoggedIn']=1;
		$_SESSION['uid']=$row['id'];
		header('Location: player.php');
	} else {
		$fail = 1;
	}
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login into WebTunes system</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      /* Override some defaults */
      html, body {
        background-color: #eee;
      }
      body {
        padding-top: 40px; 
      }
      .container {
        width: 300px;
      }

      /* The white background content wrapper */
      .container > .content {
        background-color: #fff;
        padding: 20px;
        margin: 0 -20px; 
        -webkit-border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
                border-radius: 10px 10px 10px 10px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
                box-shadow: 0 1px 2px rgba(0,0,0,.15);
      }

	  .login-form {
		margin-left: 65px;
	  }
	
	  legend {
		margin-right: -50px;
		font-weight: bold;
	  	color: #404040;
	  }

    </style>

</head>
<body>
<?
if($fail==1) {
	echo '<div class="alert alert-block alert-error">Incorrect password </div>';
}
?>
  <div class="container">
    <div class="content">
      <div class="row">
        <div class="login-form">
          <h2>WebTunes</h2>
          <form action="index.php" method="post">
            <fieldset>
              <div class="clearfix">
                <input type="text" name="login" placeholder="Login">
              </div>
              <div class="clearfix">
                <input type="password" name="pass" placeholder="Password">
              </div>
              <button class="btn" type="submit">Login</button>
  
                          </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div> <!-- /container -->
</body>
</html>