function makeStrings() {
	window.locale.appname = 'WebTunes';
	window.locale.vername = 'beta';
	window.locale.credit = 'WebTunes Teamによって日本語訳。';
	
	// --------- Error and messages
	window.locale.errors.guestUpload = 'アップロードはログインが必要です。';
	window.locale.errors.uploadSuccess = 'アップロードが終了';
	window.locale.errors.uploadFail = 'アップロードエラー';
	window.locale.errors.login = 'WebTunesにログインために「OK」をクリックしてください';
	window.locale.errors.albumSaved = "アルバムを保存しました";
	window.locale.errors.linkError = 'アルバムやカテゴリへのリンクを作ることのみ可能。';
	window.locale.errors.saved = '保存しました';
	window.locale.errors.uploading = 'アップロードが開始されました。';
	window.locale.errors.notYourList = '自分のプレイリストのみを編集することができます。';
	window.locale.errors.fail = "エラー";
	window.locale.errors.queueIsUndefined = 'システムエラー：　キューオブジェクトがありません。';
	window.locale.errors.playError = '再生エラー：';
	window.locale.errors.deleted = '削除しました';
	window.locale.errors.DLStart = "ダウンロードが開始されました";
	window.locale.errors.DLStartSC = "ダウンロードが開始されています・・・";
	
	// --------- UI strings
	window.locale.ui.uploading = "アップロード進捗...";
	window.locale.ui.wait = 'お待ちください・・・';
	window.locale.ui.cloudStream = "SoundCloudからストリーミング";
	window.locale.ui.buffering = "バッファリング・・・";
	window.locale.ui.bufferingStalled = "ストリーミングが行き詰まった・・・";
	window.locale.ui.soundCloudBeta = "SoundCloudの実験的の統合";
	window.locale.ui.onSC = " のSoundCloud";
	window.locale.ui.diskSpace = "空きディスク容量: ";
	window.locale.ui.artist = 'アーティスト';
	window.locale.ui.album = 'アルバム';
	window.locale.ui.compilation = 'それは編纂です';
	window.locale.ui.songNameUnkn = '不明な音楽';
	window.locale.ui.songBandUnkn = '不明なアーティスト';
	window.locale.ui.songAlbumUnkn= '不明なアルバム';
	window.locale.ui.shareTune = 'その音楽を送信';
	window.locale.ui.keepTune = 'その音楽をダウンロード';
	window.locale.ui.usercontrol = 'ユーザーコントロール';
	window.locale.ui.linkImport = 'シンボリックリンクインポート';
	window.locale.ui.linkTrack = '音楽シンボリックリンクを作る';
	window.locale.ui.searchCloud = 'SoundCloudで検索';
	window.locale.ui.cloudFeed = 'フィード';
	window.locale.ui.cloudFavs = 'お気に入り';
	window.locale.ui.cloudOwn = 'アップロード';
	window.locale.ui.cloudSearch = '音楽の検索';
	window.locale.ui.about = "ついて";
	window.locale.ui.shuffle = "シャッフル";
	window.locale.ui.logon = "ログイン";
	window.locale.ui.logoff= "ログオフ";
	window.locale.ui.rename = "リネーム";
	window.locale.ui.share = "送信";

	// --------- Dialog strings
	window.locale.dialogs.albumSave = "アルバムを保存する";
	window.locale.dialogs.albumDiscard = "アルバムを削除する";
	window.locale.dialogs.linkTrack = "トラックを保存する";
	window.locale.dialogs.cancel = "キャンセル";
	window.locale.dialogs.plsRename = 'プレイリストの名前を入力してください';
	window.locale.dialogs.copyLink = 'プレイリストを送信ために、そのリンクをコピーしてください。受信者は聞くことができるようになります。プレイリストをコピーこともできるようになります。';
	window.locale.dialogs.copyLinkTrk = '音楽を送信ために、そのリンクをコピーしてください。受信者はそのプライリスと/アルバムを聞くことができるようになります、しかし、その音楽は自動的に起動します。';
	window.locale.dialogs.trash = 'ファイルを削除ために、このアイコンにファイルをドラッグしてください';
	
	// --- Multipart String 'Really delete playlist NAME? To delete....'
	window.locale.dialogs.del1 = '本当に「';
	window.locale.dialogs.del2 = '」のプレイリストを削除する？音楽を一つずつ除外するには、プライリストからゴミ箱にファイルをドラッグしてください。';
	// --- End multipart string
	
	window.locale.dialogs.newList = '新しいプレイリストの名前を入力してください。';
	window.locale.dialogs.unamedList = '不明のプレイリスト';
	
	window.locale.dialogs.newSClist = '新しいSoundCloudプレイリストの名前を入力してください';
	window.locale.dialogs.unamedSClist = '不明のSoundCloudプレイリスト';
	
	
	// --------- Sidebar strings
	window.locale.sidebar.library = "ライブラリー";
	window.locale.sidebar.music = "音楽";
	window.locale.sidebar.compilations = "コンピレーション";
	window.locale.sidebar.unsorted = "ソートされない";
	window.locale.sidebar.playlists = "プレイリスト";
	window.locale.sidebar.soundcloud = 'SoundCloud';
	window.locale.sidebar.albums = 'アルバム';
}
