<? require 'conf.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <title><? echo $soft; ?> installation</title>
    <meta charset="utf-8">
	<meta name="author" content="Vladislav Korotnev">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <!-- Bootstrap -->
        <script src="http://code.jquery.com/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
 
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <link href="style.css" rel="stylesheet" media="screen">
    <script type="text/javascript">
    function rotoscope1() {
	    document.getElementById("brand").style.left="82%";
	    setTimeout(rotoscope2, 10000);
    }
     function rotoscope2() {
	    document.getElementById("brand").style.left="0%";
	    setTimeout(rotoscope1, 10000);
    }
    
    // Welcomes
    var cur=0;
    var Welc = ["Welcome","ようこそ","Добро пожаловать", "Bienvenue", "Добре дошли","Willkommen","Καλώς Ορίσατε","환영합니다", "Tervetuloa", "Hoş geldiniz"];
    var curt;
    function trans1() {
	    document.getElementById("welcome").style.opacity="0"; document.getElementById("welcome").style.left="50px";
	    curt = setTimeout(trans2, 500);
   }
   function trans2() {
	   document.getElementById("welcome").style.left="-50px";
	   if (cur == Welc.length-1) { 
		   cur = 0;
	   } else {
		   cur=cur+1;
	   }
	   document.getElementById("welcome").innerHTML=Welc[cur];
	   curt = setTimeout(trans3, 300);
   }
   function trans3() {
	   document.getElementById("welcome").style.opacity="1"; document.getElementById("welcome").style.left="0px";
	   curt = setTimeout(trans1, 2000);
   }
   function transitionFromTo(from, to) {
	   document.getElementById(from).style.left="50px";
	   document.getElementById(from).style.opacity="0";
	document.getElementById(from).style.zIndex=0;
	   document.getElementById(to).style.left="0px";
	   document.getElementById(to).style.opacity="1";
	  document.getElementById(to).style.zIndex=100;
	   clearTimeout(curt);
   }

   function noInfo() {
	   	document.getElementById("infobar").style.top="-30%";
	   	setTimeout(function() { document.getElementById("infobar").style.opacity="0"; }, 500);
   }
   function showInfo(text, extratext, briefly) {
	   document.getElementById("infotext").innerHTML=text;
	   document.getElementById("infobar").style.opacity="1";
	   document.getElementById("infobar").style.top="0px";
	   if(extratext == '' || extratext == undefined) {
		   document.getElementById("extendedText").style.display="none";
	   } else {
		    document.getElementById("extendedText").innerHTML=extratext;
		   document.getElementById("extendedText").style.display="block";
	   }
	  if (briefly == true || briefly == undefined) { 
	  	setTimeout(noInfo, 1500); 
	  }
   }
   var toTemp = "";
   function transProgress(to) {
   toTemp=to;
	   document.getElementById("progress").style.left = "15px";
	   document.getElementById("progress").style.opacity="0";
	   setTimeout(function(){
		   document.getElementById("progress").innerHTML=toTemp;
		   document.getElementById("progress").style.left = "-15px";
		   setTimeout(function(){
		   		document.getElementById("progress").style.left = "0";
		   		document.getElementById("progress").style.opacity="1";
		   }, 300);
	   } , 300);
   }
   function setProgress(to) {
	   document.getElementById("sosiska").style.width = to;
   }
    $(document).ready(function() { 
    //showInfo('Powered by vladkorotnev PHPAwesomeInstaller');
    rotoscope1(); trans1();
  
            $('#preconf').ajaxForm( {
            error:function(e) { 
            	showInfo('Server error '+e.status.toString());
            	 transitionFromTo('installing-now', 'step1')
            	 setProgress("100%");
            	 transProgress("Initializing...");
            }
            
            ,
            beforeSubmit: function() {
	            transProgress("Configuring...");
	            setProgress("30%");
            }
            ,
            success: function(data) { 
             var recv = jQuery.parseJSON(data);
               
               if(recv['status'] == 'ok') {
	              transProgress("Database setup...");
	              setProgress("50%");
	              $.post('install.php', function(data) { setProgress("100%"); transProgress("Database installed"); setTimeout( transitionFromTo('installing-now','done'), 1000)}).fail(function(e) { 
		              	transProgress('OH SHIT!');
		              	setProgress("0%");
	              showInfo('Critical error '+e.status.toString(),'Check logs and try again',false); });
               }
               if(recv['status'] == 'err') {
	               showInfo('Error! '+recv['brief'],recv['resolve'],false);
	               transitionFromTo('installing-now', 'step1')
	               transProgress("Initializing...");
	               	setProgress("100%");
               }
               
            }}); 
    
      });
    

    
  </script>
  </head>
  <body>
  <div class="navbar navbar-inverse navbar-fixed-top info-overlay" id="infobar">
	    <div class="navbar-inner">

    <center> <span class="brand infotext" id="infotext" href="#"></span><p id="extendedText" class="extrainfo" style="display:none"> asdfgh</p> <a href="#" id="infoChevron"  onclick="noInfo()"><i class="icon-chevron-up icon-white"></i></a></center>

  </div>
  </div>
    <div class="navbar">
	    <div class="navbar-inner">

    <center> <a class="brand" id="brand" href="#"><? echo $soft.' installer';  ?></a> </center>

  </div>
  </div>
<div id="properContent" class="proper-content" style=" width: 760px; margin-left: auto; margin-right:auto;">
	<center>
	<div class="pendal">
	<div class="hero-unit panel-goable" id="hero" style="height:200px;display:block; float:left; z-index: 100;"  align="left">
	  <h1 id="welcome" class="welcome">Welcome</h1>
	  <p><? echo $welcome_tagline; ?></p>
	  <p>
	    <a class="btn btn-primary btn-large" onclick="transitionFromTo('hero','step1')">
	      <? echo $beginInstall; ?>
	    </a>
	  </p>
	</div></div></center>
	<div class="pendal"><div class="container panel-goable" id="step1" style="left:-50px; opacity:0;display:block;top:-0px; z-index: 0;">
		<div class="content panel-goable">
			<h1><? echo $preconf_title; ?></h1>
			<form id="preconf" method="POST" action="<? echo $preconf_processor; ?>">
				<center><table>
				<tbody>
					<? echo genInputsPreconf(); ?>
				</tbody>
				</table></center>
				<input type="submit" class="btn btn-primary" onclick=" transitionFromTo('step1','installing-now')" value="<? echo $submit; ?>">
			</form>
		</div>
	</div></div>
	<div class="pendal"><div class="container panel-goable" id="installing-now" style="left:-50px;display:block; float:left;  opacity:0; top:0px; z-index: 0;">
		<div class="content panel-goable">
			<h1><? echo $installation_title; ?></h1>
			<p><? echo $installation_tagline; ?></p>
			<center><span class="progress-label" id="progress">Initializing...</span></center>
			<center><div class="progress progress-striped active">
			  <div class="bar" id="sosiska" style="width: 100%;"></div>
			</div></center>
		</div>
	</div></div>
	<div class="pendal"><div class="container panel-goable" id="done" style="left:-50px;display:block; float:left;  opacity:0; top:0px; z-index: 0;">
		<div class="content panel-goable">
			<h1><? echo $done_title; ?></h1>
			<p><? echo $done_tagline; ?></p>
			<center></div>
			</div></center>
		</div>
	</div></div>
</div>

      <!-- FOOTER -->
      <footer>
        <p><center><small>a vladkorotnev software product. </small></center></p>
      </footer>



  </body>
</html>