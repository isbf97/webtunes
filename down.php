<?

	/* This will download a file from the server, despite it being an audio file */
	
	$url = $_GET['file'];
	$sp = explode('/', $url);
	foreach ($sp as &$s) {
		if ($s == '..' || $s == '.'){
			die('Nope!');
		}
	}
	
	if (file_exists($url)) {
		   header('Content-Description: File Transfer');
		   header('Content-Type: application/octet-stream');
		   header('Content-Disposition: attachment; filename='.basename($url));
		   header('Content-Transfer-Encoding: binary');
		   header('Expires: 0');
		   header('Cache-Control: must-revalidate');
		   header('Pragma: public');
		   header('Content-Length: ' . filesize($url));
		   ob_clean();
		   flush();
		   readfile($url);
		   exit;
	}


?>